import 'package:Covid_Diary/Animation/FadeAnimation.dart';
import 'package:Covid_Diary/bloc.navigation_bloc/navigation_bloc.dart';
import 'package:carousel_slider/carousel_slider.dart';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:url_launcher/url_launcher.dart';

class HomePage extends StatefulWidget with NavigationStates {
  @override
  _HomePage createState() => _HomePage();
}

List<String> titles = List();

List<String> imagenames = List();
void initializeData() {
  titles.add("Fever");
  imagenames.add("assets/images/1.png");

  titles.add("Dry Cough");
  imagenames.add("assets/images/2.png");

  titles.add("Headache");
  imagenames.add("assets/images/3.png");

  titles.add("Breathless");
  imagenames.add("assets/images/4.png");
}

var imgList = [
  "assets/images/a4.png",
  "assets/images/a6.png",
  "assets/images/a9.png",
  "assets/images/a10.png",
];
// Title List Here
var titleList = [
  "WASH",
  "COVER",
  "ALWAYS",
  "USE",
];

// Description List Here
var descList = [
  "hands often.",
  "your cough.",
  "clean.",
  "mask.",
];
Widget getScreen(index) {
  return new ListView(
    physics: NeverScrollableScrollPhysics(),
    children: <Widget>[
      new Container(
        height: 90.0,
        margin: const EdgeInsets.all(20),
        child: Image.asset(
          imagenames.elementAt(index),
        ),
      ),
      new Container(
        height: 45.0,
        margin: const EdgeInsets.fromLTRB(20.0, 8.0, 20.0, 0.0),
        child: Text(
          titles.elementAt(index),
          textAlign: TextAlign.center,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(fontSize: 20),
        ),
      ),
    ],
  );
}

class _HomePage extends State<HomePage> {
  PageController _page;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _page = PageController(
      initialPage: 0,
    );
  }

  @override
  Widget build(BuildContext context) {
    initializeData();
    return Scaffold(
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
            child: Container(
          child: Column(
            children: [
              Container(
                height: 400,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    alignment: Alignment.topCenter,
                    image: AssetImage("assets/images/background.png"),
                    fit: BoxFit.fill,
                  ),
                ),
                child: Container(
                  padding: EdgeInsets.only(top: 15, bottom: 30),
                  child: Stack(
                    children: <Widget>[
                      _buildHeader(context),
                      Positioned(
                        left: 240,
                        width: 80,
                        height: 80,
                        child: FadeAnimation(
                            1.3,
                            Container(
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: AssetImage(
                                          'assets/images/light-2.png'))),
                            )),
                      ),
                      Positioned(
                        left: 270,
                        width: 80,
                        height: 120,
                        child: FadeAnimation(
                            1.3,
                            Container(
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: AssetImage(
                                          'assets/images/light-2.png'))),
                            )),
                      ),
                    ],
                  ),
                ),
              ),
              Stack(
                children: [
                  Container(
                    child: Column(children: <Widget>[
                      Text(
                        "Symptoms of ",
                        textAlign: TextAlign.right,
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 19,
                            fontFamily: 'Chilanka',
                            fontWeight: FontWeight.w900),
                      ),
                      Container(
                        child: CarouselSlider(
                          options: CarouselOptions(
                            enlargeCenterPage: true,
                            autoPlay: true,
                            autoPlayCurve: Curves.fastOutSlowIn,
                            enableInfiniteScroll: true,
                            autoPlayAnimationDuration:
                                Duration(microseconds: 800),
                            viewportFraction: 0.9,
                            height: 180,
                          ),
                          items: [
                            Container(
                              margin: EdgeInsets.all(5),
                              width: 130.0,
                              height: 50.0,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15.0),
                                image: DecorationImage(
                                  image: AssetImage("assets/images/1.png"),
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                            Container(
                                width: 130.0,
                                height: 50.0,
                                margin: EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15.0),
                                  image: DecorationImage(
                                    image: AssetImage("assets/images/2.png"),
                                    fit: BoxFit.cover,
                                  ),
                                )),
                            Container(
                                width: 130.0,
                                height: 50.0,
                                margin: EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15.0),
                                  image: DecorationImage(
                                    image: AssetImage("assets/images/3.png"),
                                    fit: BoxFit.cover,
                                  ),
                                )),
                            Container(
                                width: 130.0,
                                height: 50.0,
                                margin: EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15.0),
                                  image: DecorationImage(
                                    image: AssetImage("assets/images/4.png"),
                                    fit: BoxFit.cover,
                                  ),
                                ))
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      Text(
                        "Prevention",
                        textAlign: TextAlign.right,
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 19,
                            fontFamily: 'Chilanka',
                            fontWeight: FontWeight.w900),
                      ),
                      Container(
                        height: (70),
                        alignment: Alignment.center,
                        child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: imgList.length,
                          itemBuilder: (context, index) {
                            return GestureDetector(
                              onTap: () {
                                // This Will Call When User Click On ListView Item
                                showDialogFunc(context, imgList[index],
                                    titleList[index], descList[index]);
                              },
                              // Card Which Holds Layout Of ListView Item
                              child: Card(
                                child: Row(
                                  children: <Widget>[
                                    Container(
                                      width: (100),
                                      height: (90),
                                      decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius: BorderRadius.all(
                                            Radius.circular(15),
                                          ),
                                          border:
                                              Border.all(color: Colors.white),
                                          boxShadow: [
                                            BoxShadow(
                                              color: Colors.indigo[200],
                                              offset: Offset(1, 1),
                                              spreadRadius: 1,
                                              blurRadius: 1,
                                            ),
                                          ],
                                          gradient: LinearGradient(
                                            colors: [
                                              Colors.indigo[200],
                                              Colors.white,
                                            ],
                                          )),
                                      child: Image.asset(
                                        imgList[index],
                                      ),
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: <Widget>[
                                        Text(
                                          titleList[index],
                                          style: TextStyle(
                                            fontSize: 20,
                                            color: Colors.indigo[400],
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Container(
                                          decoration: BoxDecoration(
                                            color: Colors.transparent,
                                            image: DecorationImage(
                                              image: AssetImage(
                                                  "assets/images/virus2.png"),
                                              fit: BoxFit.cover,
                                            ),
                                            borderRadius: BorderRadius.only(
                                              bottomLeft: Radius.circular(25),
                                              bottomRight: Radius.circular(25),
                                            ),
                                          ),
                                          width: 100,
                                          child: Text(
                                            descList[index],
                                            maxLines: 3,
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontSize: 15,
                                                color: Colors.indigo[200]),
                                          ),
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      GestureDetector(
                        onTap: () {
                          BlocProvider.of<NavigationBloc>(context)
                              .add(NavigationEvents.StatClickedEvent);
                        },
                        child: Container(
                          height: (90),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(
                              Radius.circular(15),
                            ),
                            border: Border.all(color: Colors.white),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.white,
                                offset: Offset(1, 1),
                                spreadRadius: 1,
                                blurRadius: 1,
                              )
                            ],
                          ),
                          padding: EdgeInsets.all(12),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Image.asset("assets/images/map.png"),
                              RichText(
                                text: TextSpan(
                                  text: "CASES\n",
                                  style: TextStyle(
                                    color: Colors.indigo,
                                    fontWeight: FontWeight.bold,
                                    height: (1.3),
                                  ),
                                  children: [
                                    TextSpan(
                                      text: "Overview Worldwide\n",
                                      style: TextStyle(
                                        color: Colors.black87,
                                        fontWeight: FontWeight.normal,
                                      ),
                                    ),
                                    TextSpan(
                                      text: "51.118.594 confirmed",
                                      style: TextStyle(
                                        color: Colors.black87,
                                        fontWeight: FontWeight.normal,
                                        fontSize: 10,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Expanded(child: SizedBox()),
                              IconButton(
                                icon: Icon(Icons.arrow_forward_ios),
                                onPressed: null,
                              ),
                            ],
                          ),
                          margin: EdgeInsets.symmetric(horizontal: 16),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                    ]),
                  ),
                ],
              ),
            ],
          ),
        )));
  }
}

Widget _buildHeader(BuildContext context) {
  var size = MediaQuery.of(context).size;

  return Container(
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(height: (50)),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 60),
          child: RichText(
              text: TextSpan(
            text: "Fight Covid-19 ",
            style: TextStyle(
                color: Colors.lightBlue,
                fontSize: 30,
                fontFamily: 'Chilanka',
                fontWeight: FontWeight.w900),
          )),
        ),
        SizedBox(height: (25)),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 60),
          child: Text(
            "Coronavirus Relief Fund",
            style: TextStyle(
              color: Colors.black,
              fontFamily: 'Cardo',
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        SizedBox(height: (20)),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 60),
          child: Text(
            "To this fund will help to stop the virus's spread and give\ncommunitieson the font lines.",
            style: TextStyle(
              color: Colors.white,
              fontFamily: 'Cardo',
              height: (1.5),
            ),
          ),
        ),
        SizedBox(height: (25)),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: Row(
            children: <Widget>[
              SizedBox(width: (20)),
              Expanded(
                child: RaisedButton(
                  color: Colors.blue,
                  onPressed: () {
                    BlocProvider.of<NavigationBloc>(context)
                        .add(NavigationEvents.SuiviClickedEvent);
                  },
                  child: Text(
                    "DAILY",
                    style: TextStyle(color: Colors.white),
                  ),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(50),
                    ),
                  ),
                  padding: EdgeInsets.symmetric(vertical: 16),
                ),
              ),
              SizedBox(width: 20),
              Expanded(
                child: RaisedButton(
                  color: Colors.red,
                  onPressed: () => launch("tel:21267061"),
                  child: Text(
                    "EMERGENCY",
                    style: TextStyle(color: Colors.white),
                  ),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(50),
                    ),
                  ),
                  padding: EdgeInsets.symmetric(vertical: 16),
                ),
              ),
            ],
          ),
        ),
      ],
    ),
  );
}

showDialogFunc(context, img, title, desc) {
  return showDialog(
    context: context,
    builder: (context) {
      return Center(
        child: Material(
          type: MaterialType.transparency,
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: Colors.white,
            ),
            padding: EdgeInsets.all(15),
            height: 320,
            width: MediaQuery.of(context).size.width * 0.7,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(
                        Radius.circular(15),
                      ),
                      border: Border.all(color: Colors.white),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black12,
                          offset: Offset(1, 1),
                          spreadRadius: 1,
                          blurRadius: 1,
                        ),
                      ],
                      gradient: LinearGradient(
                        colors: [
                          Colors.indigo[400],
                          Colors.white,
                        ],
                      )),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(5),
                    child: Image.asset(
                      img,
                      width: 200,
                      height: 200,
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  title,
                  style: TextStyle(
                    fontSize: 25,
                    color: Colors.indigo[400],
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  // width: 200,
                  child: Align(
                    alignment: Alignment.center,
                    child: Text(
                      desc,
                      maxLines: 3,
                      style: TextStyle(fontSize: 15, color: Colors.indigo[200]),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    },
  );
}
