import 'package:Covid_Diary/services/auth.dart';
import 'package:Covid_Diary/shared/loading.dart';

import 'package:Covid_Diary/sidebar/sidebar_layout.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'dart:io';
import 'package:path/path.dart';
import '../bloc.navigation_bloc/navigation_bloc.dart';
import '../size_service.dart';

class EditProfilePage extends StatefulWidget with NavigationStates {
  @override
  _EditProfilePageState createState() => _EditProfilePageState();
}

@override
class _EditProfilePageState extends State<EditProfilePage> {
  File _image;
  String passwordNew = '';
  String passwordValid = '';
  final AuthService _auth = AuthService();
  String error = '';
  String valid = '';
  bool _isVisible = false;
  bool _isVisible2 = false;
  bool loading = false;
  bool _showPassword = false;

  var _formKey = GlobalKey<FormState>();
  Future getImage() async {
    // ignore: deprecated_member_use
    var pickImage = ImagePicker.pickImage(source: ImageSource.gallery);
    var image = await pickImage;

    setState(() {
      _image = image;
      print('Image Path $_image');
    });
  }

  Future uploadImageToFirebase(BuildContext context) async {
    var user = await FirebaseAuth.instance.currentUser();

    String fileName = basename(_image.path);
    StorageReference firebaseStorageRef =
        FirebaseStorage.instance.ref().child('${user.uid}');
    StorageUploadTask uploadTask = firebaseStorageRef.putFile(_image);
    StorageTaskSnapshot taskSnapshot = await uploadTask.onComplete;
    taskSnapshot.ref.getDownloadURL().then(
          (value) => print("Done: $value"),
        );
    setState(() {
      print("Profile Picture uploaded");
      Scaffold.of(context)
          .showSnackBar(SnackBar(content: Text('Profile Picture Uploaded')));
    });
  }

  Future uploadPic(BuildContext context) async {
    var user = await FirebaseAuth.instance.currentUser();
    print(user.uid);
    FirebaseStorage.instance.ref().child('${user.uid}');

    setState(() {
      print("Profile Picture uploaded");
      Scaffold.of(context)
          .showSnackBar(SnackBar(content: Text('Profile Picture Uploaded')));
    });
  }

  @override
  Widget build(BuildContext context) {
    initSizeConfigGlobal(context);
    Size size = MediaQuery.of(context).size;

    return loading
        ? Loading()
        : Scaffold(
            backgroundColor: Colors.white,
            body: Container(
                padding: EdgeInsets.only(
                    top: getSizeVerticallyScaled(25),
                    right: getSizeVerticallyScaled(3)),
                child: Stack(
                  children: [
                    Positioned(
                      top: 0,
                      left: 0,
                      child: Image.asset(
                        "assets/images/main_top.png",
                        width: size.width * 0.3,
                      ),
                    ),
                    Positioned(
                      bottom: 0,
                      left: 0,
                      child: Image.asset(
                        "assets/images/main_bottom.png",
                        width: size.width * 0.2,
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        FocusScope.of(context).unfocus();
                      },
                      child: ListView(
                        children: [
                          Text(
                            "Edit Profile",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Colors.lightBlue,
                                fontSize: 30,
                                fontFamily: 'Chilanka',
                                fontWeight: FontWeight.w900),
                          ),
                          SizedBox(
                            height: getSizeVerticallyScaled(25),
                          ),
                          Center(
                            child: Stack(
                              children: [
                                Container(
                                  child: CircleAvatar(
                                    radius: 70,
                                    backgroundColor: Color(0xff476cfb),
                                    child: ClipOval(
                                      child: new SizedBox(
                                        width: 180.0,
                                        height: 180.0,
                                        child: (_image != null)
                                            ? Image.file(
                                                _image,
                                                fit: BoxFit.fill,
                                              )
                                            : Image.network(
                                                "https://images.pexels.com/photos/3307758/pexels-photo-3307758.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=250",
                                                fit: BoxFit.fill,
                                              ),
                                      ),
                                    ),
                                  ),
                                ),
                                Positioned(
                                    bottom: getSizeVerticallyScaled(0),
                                    right: getSizeVerticallyScaled(0),
                                    child: Container(
                                      height: getSizeVerticallyScaled(50),
                                      width: getSizeVerticallyScaled(45),
                                      child: IconButton(
                                        icon: Icon(
                                          FontAwesomeIcons.camera,
                                          color: Colors.black,
                                          size: 30.0,
                                        ),
                                        onPressed: () {
                                          getImage();
                                        },
                                      ),
                                    )),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: getSizeVerticallyScaled(35),
                          ),
                          Column(
                            children: [
                              RaisedButton(
                                color: Color(0xff476cfb),
                                onPressed: () {
                                  uploadImageToFirebase(context);
                                  setState(() {
                                    valid = 'Picture Changed Please Sign out';
                                    _isVisible = false;
                                    _isVisible2 = true;
                                    // _auth.signout();
                                    // loading = true;
                                  });
                                },
                                elevation: 4.0,
                                splashColor: Colors.blueGrey,
                                child: Text(
                                  'Submit',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 16.0),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: getSizeVerticallyScaled(35),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(15.0),
                            child: Form(
                              key: _formKey,
                              child: Column(
                                children: <Widget>[
                                  TextFormField(
                                      obscureText: !this._showPassword,
                                      validator: (value) => value.length < 6
                                          ? 'Enter a password 6+ chars long'
                                          : null,
                                      onChanged: (val) {
                                        setState(() => passwordNew = val);
                                      },
                                      textAlign: TextAlign.left,
                                      autocorrect: true,
                                      decoration: InputDecoration(
                                        prefixIcon:
                                            Icon(Icons.security_rounded),
                                        suffixIcon: IconButton(
                                          icon: Icon(
                                            Icons.remove_red_eye,
                                            color: this._showPassword
                                                ? Colors.blue
                                                : Colors.grey,
                                          ),
                                          onPressed: () {
                                            setState(() => this._showPassword =
                                                !this._showPassword);
                                          },
                                        ),

                                        isDense: true, // Added this
                                        contentPadding: EdgeInsets.all(
                                            getSizeVerticallyScaled(20)),
                                        labelText: " Password",

                                        hintText: " New Password",
                                        hintStyle: TextStyle(
                                            fontSize: 18,
                                            color: Colors.grey[350],
                                            fontWeight: FontWeight.w600),

                                        focusColor: Color(0xff0962ff),
                                        focusedBorder: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          borderSide: BorderSide(
                                              color: Color(0xff0962ff)),
                                        ),
                                        enabledBorder: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(20),
                                          borderSide: BorderSide(
                                            color: Colors.grey[350],
                                          ),
                                        ),
                                      )),
                                  SizedBox(
                                    height: getSizeVerticallyScaled(35),
                                  ),
                                  TextFormField(
                                      obscureText: !this._showPassword,
                                      validator: (value) => value.length < 6
                                          ? 'Enter a password 6+ chars long'
                                          : null,
                                      onChanged: (val) {
                                        setState(() => passwordValid = val);
                                      },
                                      textAlign: TextAlign.left,
                                      autocorrect: true,
                                      decoration: InputDecoration(
                                        prefixIcon:
                                            Icon(Icons.security_rounded),
                                        suffixIcon: IconButton(
                                          icon: Icon(
                                            Icons.remove_red_eye,
                                            color: this._showPassword
                                                ? Colors.blue
                                                : Colors.grey,
                                          ),
                                          onPressed: () {
                                            setState(() => this._showPassword =
                                                !this._showPassword);
                                          },
                                        ),

                                        isDense: true, // Added this
                                        contentPadding: EdgeInsets.all(
                                            getSizeVerticallyScaled(20)),
                                        labelText: " Password",

                                        hintText: " Repeat Password",
                                        hintStyle: TextStyle(
                                            fontSize: 18,
                                            color: Colors.grey[350],
                                            fontWeight: FontWeight.w600),

                                        focusColor: Color(0xff0962ff),
                                        focusedBorder: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          borderSide: BorderSide(
                                              color: Color(0xff0962ff)),
                                        ),
                                        enabledBorder: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(20),
                                          borderSide: BorderSide(
                                            color: Colors.grey[350],
                                          ),
                                        ),
                                      )),
                                  SizedBox(
                                    height: getSizeVerticallyScaled(35),
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      OutlineButton(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 50),
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(20)),
                                        onPressed: () {
                                          setState(() {
                                            valid = '';
                                            _isVisible = false;
                                            _isVisible2 = false;
                                            // _auth.signout();
                                            // loading = true;
                                          });
                                          Navigator.of(context).push(
                                            MaterialPageRoute(
                                              builder: (_) => SideBarLayout(),
                                            ),
                                          );
                                        },
                                        child: Text("CANCEL",
                                            style: TextStyle(
                                                fontSize: 14,
                                                letterSpacing: 2.2,
                                                color: Colors.black)),
                                      ),
                                      RaisedButton(
                                        onPressed: () async {
                                          if (_formKey.currentState
                                              .validate()) {
                                            if (passwordNew != passwordValid) {
                                              setState(() {
                                                error = 'Password not the some';
                                                _isVisible = true;
                                              });
                                            } else {
                                              _auth.newpsw(passwordValid);
                                              setState(() {
                                                valid =
                                                    'Password Changed Please Sign out';
                                                _isVisible = false;
                                                _isVisible2 = true;
                                                // _auth.signout();
                                                // loading = true;
                                              });
                                              //_auth.signout();
                                              //Navigator.of(context).push(
                                              //MaterialPageRoute(
                                              //builder: (_) => Login(),
                                              //),
                                              //);
                                            }
                                          }
                                        },
                                        color: Colors.lightBlue,
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 50),
                                        elevation: 2,
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(20)),
                                        child: Text(
                                          "SAVE",
                                          style: TextStyle(
                                              fontSize: 14,
                                              letterSpacing: 2.2,
                                              color: Colors.white),
                                        ),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ),
                          SizedBox(
                            height: getSizeVerticallyScaled(30),
                          ),
                          Visibility(
                            visible: _isVisible,
                            child: Row(children: <Widget>[
                              Text(
                                error,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.red,
                                    fontSize: 14,
                                    fontFamily: 'Product Sans'),
                              ),
                              Icon(
                                Icons.warning,
                                color: Colors.red.shade400,
                              ),
                            ]),
                          ),
                          Visibility(
                            visible: _isVisible2,
                            child: Row(children: <Widget>[
                              Text(
                                valid,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.green,
                                    fontSize: 14,
                                    fontFamily: 'Product Sans'),
                              ),
                              Icon(
                                Icons.done,
                                color: Colors.green.shade400,
                              ),
                            ]),
                          ),
                        ],
                      ),
                    ),
                  ],
                )),
          );
  }
}
