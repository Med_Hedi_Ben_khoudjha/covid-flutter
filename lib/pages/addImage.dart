import 'package:Covid_Diary/services/auth.dart';
import 'package:Covid_Diary/shared/loading.dart';
import 'package:Covid_Diary/sidebar/sidebar_layout.dart';
import 'package:Covid_Diary/user/login.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:io';
import '../size_service.dart';
import 'dart:async';
import 'package:path/path.dart';

class AddImage extends StatefulWidget {
  @override
  _AddImage createState() => _AddImage();
}

class _AddImage extends State<AddImage> {
  final AuthService _auth = AuthService();
  final _formkey = GlobalKey<FormState>();
  bool _showPassword = false;
  File _image;
  String email = '';
  String name = '';
  String password = '';
  String error = '';
  bool loading = false;
  Future getImage() async {
    // ignore: deprecated_member_use
    var pickImage = ImagePicker.pickImage(source: ImageSource.gallery);
    var image = await pickImage;

    setState(() {
      _image = image;
      print('Image Path $_image');
    });
  }

  Future uploadImageToFirebase(BuildContext context) async {
    var user = await FirebaseAuth.instance.currentUser();

    String fileName = basename(_image.path);
    StorageReference firebaseStorageRef =
        FirebaseStorage.instance.ref().child('${user.uid}');
    StorageUploadTask uploadTask = firebaseStorageRef.putFile(_image);
    StorageTaskSnapshot taskSnapshot = await uploadTask.onComplete;
    taskSnapshot.ref.getDownloadURL().then(
          (value) => print("Done: $value"),
        );
    setState(() {
      print("Profile Picture uploaded");
      Scaffold.of(context)
          .showSnackBar(SnackBar(content: Text('Profile Picture Uploaded')));
    });
  }

  Future uploadPic(BuildContext context) async {
    var user = await FirebaseAuth.instance.currentUser();
    print(user.uid);
    FirebaseStorage.instance.ref().child('${user.uid}');

    setState(() {
      print("Profile Picture uploaded");
      Scaffold.of(context)
          .showSnackBar(SnackBar(content: Text('Profile Picture Uploaded')));
    });
  }

  @override
  Widget build(BuildContext context) {
    var scrWidth = MediaQuery.of(context).size.width;
    var scrHeight = MediaQuery.of(context).size.height;
    initSizeConfigGlobal(context);

    return loading
        ? Loading()
        : SafeArea(
            child: Scaffold(
              body: SingleChildScrollView(
                physics: BouncingScrollPhysics(),
                child: Stack(
                  children: [
                    Column(
                      children: [
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 40.0, top: 40),
                            child: Text(
                              'Sign Up',
                              style: TextStyle(
                                fontFamily: 'Chilanka',
                                fontSize: 35,
                                color: Colors.black,
                                fontWeight: FontWeight.w900,
                              ),
                            ),
                            //
                          ),
                        ),

                        SizedBox(
                          height: getSizeVerticallyScaled(30),
                        ),
                        //

                        SizedBox(
                          height: getSizeVerticallyScaled(30),
                        ),
                        //
                        Container(
                            width: getSizeVerticallyScaled(400),
                            child: Form(
                                key: _formkey,
                                child: Column(
                                  children: <Widget>[
                                    Center(
                                      child: Stack(
                                        children: [
                                          Container(
                                            child: CircleAvatar(
                                              radius: 70,
                                              backgroundColor:
                                                  Color(0xff476cfb),
                                              child: ClipOval(
                                                child: new SizedBox(
                                                  width: 180.0,
                                                  height: 180.0,
                                                  child: (_image != null)
                                                      ? Image.file(
                                                          _image,
                                                          fit: BoxFit.fill,
                                                        )
                                                      : Image.network(
                                                          "https://images.pexels.com/photos/3307758/pexels-photo-3307758.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=250",
                                                          fit: BoxFit.fill,
                                                        ),
                                                ),
                                              ),
                                            ),
                                          ),
                                          Positioned(
                                              bottom:
                                                  getSizeVerticallyScaled(0),
                                              right: getSizeVerticallyScaled(0),
                                              child: Container(
                                                height:
                                                    getSizeVerticallyScaled(50),
                                                width:
                                                    getSizeVerticallyScaled(45),
                                                child: IconButton(
                                                  icon: Icon(
                                                    FontAwesomeIcons.camera,
                                                    color: Colors.black,
                                                    size: 30.0,
                                                  ),
                                                  onPressed: () {
                                                    getImage();
                                                  },
                                                ),
                                              )),
                                        ],
                                      ),
                                    ),
                                    SizedBox(
                                      height: getSizeVerticallyScaled(30),
                                    ),
                                    Text(
                                      "Creating an account means you're okay with\nour Terms of Service and Privacy Policy",
                                      style: TextStyle(
                                        fontFamily: 'Product Sans',
                                        fontSize: 13,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.indigo[200],
                                      ),
                                      //
                                    ),
                                    SizedBox(
                                      height: getSizeVerticallyScaled(30),
                                    ),
                                    InkWell(
                                        child: Container(
                                          margin: EdgeInsets.symmetric(
                                              vertical: 20),
                                          width: scrWidth * 0.5,
                                          height: getSizeVerticallyScaled(70),
                                          decoration: BoxDecoration(
                                            color: Colors.indigo[200],
                                            borderRadius:
                                                BorderRadius.circular(40),
                                          ),
                                          child: Center(
                                            child: Text(
                                              'Send',
                                              style: TextStyle(
                                                fontFamily: 'ProductSans',
                                                fontSize: 20,
                                                fontWeight: FontWeight.w100,
                                                color: Colors.white,
                                              ),
                                            ),
                                          ),
                                        ),
                                        onTap: () async {
                                          if (_formkey.currentState
                                              .validate()) {
                                            setState(() => loading = true);
                                            uploadImageToFirebase(context)
                                                .whenComplete(() async {
                                              Navigator.push(
                                                context,
                                                PageRouteBuilder(
                                                    transitionsBuilder:
                                                        (BuildContext context,
                                                            Animation<double>
                                                                animation,
                                                            Animation<double>
                                                                secanimtion,
                                                            Widget child) {
                                                      animation =
                                                          CurvedAnimation(
                                                              parent: animation,
                                                              curve: Curves
                                                                  .elasticInOut);
                                                      return ScaleTransition(
                                                        alignment:
                                                            Alignment.center,
                                                        scale: animation,
                                                        child: child,
                                                      );
                                                    },
                                                    transitionDuration:
                                                        Duration(seconds: 1),
                                                    pageBuilder:
                                                        (BuildContext context,
                                                            Animation<double>
                                                                animation,
                                                            Animation<double>
                                                                secanimtion) {
                                                      return Login();
                                                    }),
                                              );
                                            });
                                          }

                                          //Navigator.push(
                                          //context,
                                          // MaterialPageRoute(
                                          // builder: (context) => SideBarLayout()));
                                        }),
                                    SizedBox(
                                      height: getSizeVerticallyScaled(30),
                                    ),
                                    Text(
                                      error,
                                      style: TextStyle(
                                          color: Colors.red,
                                          fontSize: 14,
                                          fontFamily: 'Product Sans'),
                                    ),
                                  ],
                                ))),

                        SizedBox(
                          height: getSizeVerticallyScaled(30),
                        ),
                        //
                      ],
                    ),
                    ClipPath(
                      clipper: OuterClippedPart(),
                      child: Container(
                        color: Colors.indigo[400],
                        width: scrWidth,
                        height: scrHeight,
                      ),
                    ),
                    //
                    ClipPath(
                      clipper: InnerClippedPart(),
                      child: Container(
                        color: Colors.indigo[200],
                        width: scrWidth,
                        height: scrHeight,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
  }
}

// ignore: must_be_immutable
// ignore: camel_case_types
@immutable
class Neu_button extends StatelessWidget {
  Neu_button({this.char});
  String char;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: getSizeVerticallyScaled(50),
      height: getSizeVerticallyScaled(50),
      decoration: BoxDecoration(
        color: Color(0xffffffff),
        borderRadius: BorderRadius.circular(15),
        boxShadow: [
          BoxShadow(
            offset: Offset(12, 11),
            blurRadius: 26,
            color: Colors.lightBlue.withOpacity(0.1),
          )
        ],
      ),
      //
      child: Center(
        child: Text(
          char,
          style: TextStyle(
            fontFamily: 'ProductSans',
            fontSize: 26,
            fontWeight: FontWeight.bold,
            color: Colors.lightBlue,
          ),
        ),
      ),
    );
  }
}

class OuterClippedPart extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();
    //
    path.moveTo(size.width / 2, 0);
    path.lineTo(size.width, 0);
    path.lineTo(size.width, size.height / 4);
    //
    path.cubicTo(size.width * 0.55, size.height * 0.16, size.width * 0.85,
        size.height * 0.05, size.width / 2, 0);
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }
}

class InnerClippedPart extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();
    //
    path.moveTo(size.width * 0.7, 0);
    path.lineTo(size.width, 0);
    path.lineTo(size.width, size.height * 0.1);
    //
    path.quadraticBezierTo(
        size.width * 0.8, size.height * 0.11, size.width * 0.7, 0);

    //
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }
}
