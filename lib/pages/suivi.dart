import 'package:Covid_Diary/bloc.navigation_bloc/navigation_bloc.dart';
import 'package:Covid_Diary/services/database.dart';
import 'package:Covid_Diary/shared/save.dart';
import 'package:Covid_Diary/size_service.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:flutter_scan_bluetooth/flutter_scan_bluetooth.dart';

import 'package:geolocator/geolocator.dart';

class Suivi extends StatefulWidget with NavigationStates {
  @override
  _Suivi createState() => _Suivi();
}

class _Suivi extends State<Suivi> {
  String _dataB = '';
  bool _scanning = false;
  FlutterScanBluetooth _bluetooth = FlutterScanBluetooth();
  final List<String> imgList = [
    "assets/images/C1.png",
    "assets/images/C2.png"
        "assets/images/1.png"
        "assets/images/1.png"
        "assets/images/1.png"
  ];
  @override
  void initState() {
    super.initState();

    _getCurrentLocation();
    _bluetooth.devices.listen((device) {
      setState(() {
        _dataB += device.name + ' (${device.address})\n';
      });
    });
    _bluetooth.scanStopped.listen((device) {
      setState(() {
        _scanning = false;
        _dataB += 'scan stopped\n';
      });
    });
  }

  bool isLoading = false;

  String _locationMessage = "";
  String _city = "";
  DatabaseService _data = DatabaseService();
  var position;
  void _getCurrentLocation() async {
    position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);

    print(position);

    setState(() {
      _locationMessage = "${position.latitude}, ${position.longitude}";
    });
  }

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
        backgroundColor: Colors.white,
        body: Container(
          height: size.height,
          child: Stack(children: [
            Positioned(
              top: 0,
              left: 0,
              child: Image.asset(
                "assets/images/main_top.png",
                width: size.width * 0.3,
              ),
            ),
            Positioned(
              bottom: 0,
              left: 0,
              child: Image.asset(
                "assets/images/main_bottom.png",
                width: size.width * 0.2,
              ),
            ),
            ListView(
              children: [
                SizedBox(
                  height: getSizeVerticallyScaled(30),
                ),
                Text(
                  "Daily Monitoring",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 30,
                      color: Colors.lightBlue,
                      fontFamily: 'Chilanka',
                      fontWeight: FontWeight.w500),
                ),
                SizedBox(
                  height: getSizeVerticallyScaled(30),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 50, left: 60),
                  child: Text(
                    _dataB,
                    style: TextStyle(color: Colors.red),
                  ),
                ),
                CarouselSlider(
                  options: CarouselOptions(
                      enlargeCenterPage: true,
                      autoPlay: true,
                      autoPlayCurve: Curves.fastOutSlowIn,
                      enableInfiniteScroll: true,
                      autoPlayAnimationDuration: Duration(microseconds: 800),
                      viewportFraction: 0.8,
                      height: 180),
                  items: [
                    Container(
                      margin: EdgeInsets.all(5),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15.0),
                        image: DecorationImage(
                          image: AssetImage("assets/images/c1.jpg"),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    Container(
                        margin: EdgeInsets.all(5),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15.0),
                          image: DecorationImage(
                            image: AssetImage("assets/images/c2.jpg"),
                            fit: BoxFit.cover,
                          ),
                        )),
                    Container(
                        margin: EdgeInsets.all(5),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15.0),
                          image: DecorationImage(
                            image: AssetImage("assets/images/c3.jpg"),
                            fit: BoxFit.cover,
                          ),
                        ))
                  ],
                ),
                Container(
                  padding: const EdgeInsets.only(left: 10, top: 30),
                  child: Center(
                    child: RaisedButton(
                        child: Text(_scanning ? 'Stop scan' : 'Start scan'),
                        color: Colors.lightBlue,
                        onPressed: () async {
                          try {
                            if (_scanning) {
                              await _bluetooth.stopScan();
                              debugPrint("scanning stoped");
                            } else {
                              await _bluetooth.startScan(pairedDevices: false);
                              debugPrint("scanning started");
                              if (_dataB != '' ||
                                  identical(
                                      _dataB, "scan stopped scan stopped") ||
                                  identical(_dataB, "scan stopped") ||
                                  identical(_dataB, "scan stopped")) {
                                _data.updateUserSuivi(
                                    position.latitude.toString(),
                                    position.longitude.toString(),
                                    _dataB.toString());
                                // _data.updateUserSuivi(
                                //     position.latitude.toString(),
                                //     position.longitude.toString(),
                                //     "DESKTOP-14EKDV9 (60:D8:19:F6:62:6D)");
                                isLoading
                                    ? Save()
                                    : BlocProvider.of<NavigationBloc>(context)
                                        .add(NavigationEvents
                                            .HomePageClickedEvent);
                              } else {}
                              print(isLoading);

                              setState(() {
                                _scanning = true;
                                _dataB = '';
                              });
                            }
                          } on PlatformException catch (e) {
                            debugPrint(e.toString());
                          }
                        }),
                  ),
                ),
                SizedBox(height: (20)),
                Padding(
                  padding: const EdgeInsets.only(left: 40, top: 30),
                  child: Text(
                    "Protégeons nos proches,protegeons-nous et protegeons les autres.Avec Fight covid-19, participez à la lutte contre l'épidémie en limitant les risques de transmission",
                    overflow: TextOverflow.clip,
                    style: TextStyle(
                      color: Colors.black,
                      fontFamily: 'Cardo',
                      height: (1.5),
                    ),
                  ),
                ),
                SizedBox(height: (25)),
              ],
            ),
          ]),
        ));
  }
}
