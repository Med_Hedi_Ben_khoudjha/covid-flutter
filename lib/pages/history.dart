import 'dart:async';

import 'package:Covid_Diary/bloc.navigation_bloc/navigation_bloc.dart';

import 'package:Covid_Diary/shared/loading.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter_map_marker_popup/flutter_map_marker_popup.dart';

import 'package:latlong/latlong.dart';
import 'package:url_launcher/url_launcher.dart';

import '../size_service.dart';

class History extends StatefulWidget with NavigationStates {
  @override
  _History createState() => _History();
}

class _History extends State<History> {
  static List<Marker> _markers;
  _launchURL() async {
    const url =
        'https://indianexpress.com/article/explained/coronavirus-recovery-home-quarantine-covid-infection-6451142/';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  Widget build(BuildContext context) {
    initSizeConfigGlobal(context);

    // ignore: unused_element
    getData2() async {
      //use a Async-await function to get the data
      String userId = (await FirebaseAuth.instance.currentUser()).uid;
      print(userId);

      QuerySnapshot querySnapshot =
          await Firestore.instance.collection("suivi").getDocuments();
      var list = querySnapshot.documents;
      _markers = new List<Marker>();

      for (int i = 0; i < list.length; i++) {
        _markers.add(new Marker(
            point: LatLng(double.parse(list[i].data["latitude"]),
                double.parse(list[i].data["longitude"])),
            builder: (ctx) => Container(
                  child: IconButton(
                    icon: Icon(
                      FontAwesomeIcons.mapMarkerAlt,
                      color: Colors.red,
                      size: 40,
                    ),
                    onPressed: () {
                      print(list[i].data);
                    },
                  ),
                )));
      }
      setState(() {
        _markers.clear();
      });
    }

    //getData2();

    // getData2();
    //for (var item in _markers) {
    // print(item.point);
    //}
    DateTime time = DateTime.now();
    bool _disposed = false;

    @override
    // ignore: unused_element
    void initState() {
      Timer(Duration(seconds: 1), () {
        if (!_disposed)
          setState(() {
            time = time.add(Duration(seconds: -1));
          });
      });
      super.initState();
    }

    final PopupController _popupLayerController = PopupController();

    bool loading = false;
    // if (_markers1.isEmpty) {
    // loading = true;
    //}
    Size size = MediaQuery.of(context).size;

    return loading
        ? Loading()
        : Scaffold(
            backgroundColor: Colors.white,
            body: Container(
                height: size.height,
                child: Stack(
                  children: [
                    Positioned(
                      top: 0,
                      left: 0,
                      child: Image.asset(
                        "assets/images/main_top.png",
                        width: size.width * 0.3,
                      ),
                    ),
                    Positioned(
                      bottom: 0,
                      left: 0,
                      child: Image.asset(
                        "assets/images/main_bottom.png",
                        width: size.width * 0.2,
                      ),
                    ),
                    ListView(
                      children: [
                        new FutureBuilder<QuerySnapshot>(
                            future: Firestore.instance
                                .collection("suivi")
                                .getDocuments(),
                            builder:
                                (BuildContext context, AsyncSnapshot snapshot) {
                              _markers = new List<Marker>();
                              if (snapshot.connectionState ==
                                  ConnectionState.waiting) {
                                return new Center(
                                  child: new CircularProgressIndicator(),
                                );
                              } else if (snapshot.hasError) {
                                return new Text('Error: ${snapshot.error}');
                              } else {
                                for (int i = 0;
                                    i < snapshot.data.documents.length;
                                    i++) {
                                  DocumentSnapshot data =
                                      snapshot.data.documents[i];
                                  print(snapshot.data.documents.length);

                                  _markers.add(new Marker(
                                      height: 40,
                                      width: 40,
                                      point: LatLng(
                                          double.parse(data["latitude"]),
                                          double.parse(data["longitude"])),
                                      anchorPos:
                                          AnchorPos.align(AnchorAlign.top),
                                      builder: (ctx) => Container(
                                            child: IconButton(
                                              icon: Icon(
                                                FontAwesomeIcons.mapMarkerAlt,
                                                color: Colors.red,
                                                size: 40,
                                              ),
                                              onPressed: () {
                                                showDialogFunc(
                                                    context,
                                                    "Fight Covid-19",
                                                    data["app"]);
                                              },
                                            ),
                                          )));
                                }

                                return !snapshot.hasData
                                    ? Center(child: CircularProgressIndicator())
                                    : Column(
                                        children: [
                                          SizedBox(
                                            height: getSizeVerticallyScaled(30),
                                          ),
                                          Text(
                                            "History Daily",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontSize: 30,
                                                color: Colors.lightBlue,
                                                fontFamily: 'Chilanka',
                                                fontWeight: FontWeight.w500),
                                          ),
                                          SizedBox(
                                            height: getSizeVerticallyScaled(90),
                                          ),
                                          new InkWell(
                                              child: new Text(
                                                'The best practices for home quarantine of Covid-19 patients',
                                                textAlign: TextAlign.right,
                                                overflow: TextOverflow.fade,
                                                style: TextStyle(
                                                    color: Colors.red,
                                                    fontSize: 15,
                                                    fontFamily: 'Cardo'),
                                              ),
                                              onTap: () => launch(
                                                  'https://indianexpress.com/article/explained/coronavirus-recovery-home-quarantine-covid-infection-6451142/')),
                                          SizedBox(
                                            height: getSizeVerticallyScaled(30),
                                          ),
                                          Container(
                                            height: 350,
                                            padding: EdgeInsets.all(5),
                                            child: FlutterMap(
                                              options: new MapOptions(
                                                zoom: 12.0,
                                                center: LatLng(
                                                    37.2720587, 9.8713455),
                                                plugins: <MapPlugin>[
                                                  PopupMarkerPlugin()
                                                ],
                                                interactive: true,
                                                onTap: (_) =>
                                                    _popupLayerController
                                                        .hidePopup(),
                                              ),
                                              layers: [
                                                new TileLayerOptions(
                                                    urlTemplate:
                                                        "https://api.mapbox.com/styles/v1/medcabiste/ckhbz0r9e0zf419rj58vxyzyc/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1IjoibWVkY2FiaXN0ZSIsImEiOiJjanRudmxqNDMwZ3J6NDRuMm5qZ2Zka2F0In0.YlvlWPvJrQny19UOUWdmjA",
                                                    subdomains: [
                                                      'a',
                                                      'b',
                                                      'c'
                                                    ],
                                                    additionalOptions: {
                                                      'accessToken':
                                                          'sk.eyJ1IjoibWVkY2FiaXN0ZSIsImEiOiJja2hieHlnMDkwMm9tMnN0Z24yZWpxOWd2In0.JEHu3uLOaDbVLj_C8dLq9A',
                                                      'id': 'mapbox.streets'
                                                    }),
                                                PopupMarkerLayerOptions(
                                                  markers: _markers,
                                                  popupController:
                                                      _popupLayerController,
                                                  popupBuilder:
                                                      (_, Marker marker) {
                                                    return Card(
                                                        child: const Text(
                                                            'Fight Covid 19'));
                                                  },
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      );
                              }
                            }),
                        SizedBox(height: (20)),
                        Padding(
                          padding: const EdgeInsets.only(left: 20, top: 30),
                          child: Text(
                            "Coronaviruses are important human and animal pathogens. At the end of 2019, a novel coronavirus was identified as the cause of a cluster of pneumonia cases in Wuhan, a city in the Hubei Province of China. It rapidly spread, resulting in a global pandemic. The disease is designated COVID-19, which stands for coronavirus disease 2019",
                            overflow: TextOverflow.clip,
                            style: TextStyle(
                              color: Colors.black,
                              fontFamily: 'Cardo',
                              height: (1.5),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: getSizeVerticallyScaled(70),
                        ),
                      ],
                    ),
                  ],
                )));
  }
}

showDialogFunc(context, title, desc) {
  return showDialog(
    context: context,
    builder: (context) {
      return Center(
        child: Material(
          type: MaterialType.transparency,
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: Colors.white,
            ),
            padding: EdgeInsets.all(15),
            height: 320,
            width: MediaQuery.of(context).size.width * 0.7,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(
                        Radius.circular(15),
                      ),
                      border: Border.all(color: Colors.white),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black12,
                          offset: Offset(1, 1),
                          spreadRadius: 1,
                          blurRadius: 1,
                        ),
                      ],
                      gradient: LinearGradient(
                        colors: [
                          Colors.lightBlue,
                          Colors.white,
                        ],
                      )),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  title,
                  style: TextStyle(
                    color: Colors.black,
                    fontFamily: 'Cardo',
                    height: (1.5),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  // width: 200,
                  child: Align(
                    alignment: Alignment.center,
                    child: Text(
                      desc,
                      maxLines: 3,
                      style: TextStyle(fontSize: 15, color: Colors.lightBlue),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    },
  );
}
