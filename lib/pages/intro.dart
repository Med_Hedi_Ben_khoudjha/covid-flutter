import 'package:Covid_Diary/helpers/Strings.dart';
import 'package:Covid_Diary/models/user.dart';
import 'package:Covid_Diary/size_service.dart';

import 'package:Covid_Diary/user/wrapper.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';

class Intro extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<Intro> {
  PageController _pageController;
  int currentIndex = 0;

  @override
  void initState() {
    _pageController = PageController(initialPage: 0);
    super.initState();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);
    print(user);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        actions: <Widget>[
          Padding(
              padding: EdgeInsets.only(right: 20, top: 20),
              child: InkWell(
                child: Text(
                  "Skip",
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 12,
                      fontFamily: 'Chilanka',
                      fontWeight: FontWeight.w900),
                ),
                onTap: () {
                  Navigator.push(
                    context,
                    PageRouteBuilder(
                        transitionsBuilder: (BuildContext context,
                            Animation<double> animation,
                            Animation<double> secanimtion,
                            Widget child) {
                          animation = CurvedAnimation(
                              parent: animation, curve: Curves.elasticInOut);
                          return ScaleTransition(
                            alignment: Alignment.center,
                            scale: animation,
                            child: child,
                          );
                        },
                        transitionDuration: Duration(milliseconds: 500),
                        pageBuilder: (BuildContext context,
                            Animation<double> animation,
                            Animation<double> secanimtion) {
                          return Wrapper();
                        }),
                  );
                },
              ))
        ],
      ),
      body: Stack(
        alignment: Alignment.bottomCenter,
        children: <Widget>[
          PageView(
            onPageChanged: (int page) {
              setState(() {
                currentIndex = page;
              });
            },
            controller: _pageController,
            children: <Widget>[
              makePage(
                  image: 'assets/images/d.png',
                  title: Strings.stepOneTitle,
                  content: Strings.stepOneContent),
              makePage(
                reverse: true,
                image: 'assets/images/p.png',
                title: Strings.stepTwoTitle,
                content: Strings.stepTwoContent,
              ),
              makePage(
                  image: 'assets/images/l.png',
                  title: Strings.stepThreeTitle,
                  content: Strings.stepThreeContent),
            ],
          ),
          Container(
            margin: EdgeInsets.only(bottom: 60),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: _buildIndicator(),
            ),
          )
        ],
      ),
    );
  }

  Widget makePage({image, title, content, reverse = false}) {
    initSizeConfigGlobal(context);

    return Container(
      padding: EdgeInsets.only(left: 50, right: 50, bottom: 20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          !reverse
              ? Column(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Image.asset(image),
                    ),
                    SizedBox(
                      height: getSizeVerticallyScaled(30),
                    ),
                  ],
                )
              : SizedBox(),
          Text(
            title,
            style: TextStyle(
                color: Colors.indigo[400],
                fontSize: 30,
                fontFamily: 'Chilanka',
                fontWeight: FontWeight.w900),
          ),
          SizedBox(
            height: getSizeVerticallyScaled(10),
          ),
          Text(
            content,
            textAlign: TextAlign.justify,
            style: TextStyle(
                color: Colors.indigo[200],
                fontSize: 13,
                fontFamily: 'Chilanka',
                fontWeight: FontWeight.w900),
          ),
          reverse
              ? Column(
                  children: <Widget>[
                    SizedBox(
                      height: getSizeVerticallyScaled(30),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Image.asset(image),
                    ),
                  ],
                )
              : SizedBox(),
        ],
      ),
    );
  }

  Widget _indicator(bool isActive) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 300),
      height: getSizeVerticallyScaled(6),
      width: isActive ? 30 : 6,
      margin: EdgeInsets.only(right: 7),
      decoration: BoxDecoration(
          color: Colors.indigo[400], borderRadius: BorderRadius.circular(5)),
    );
  }

  List<Widget> _buildIndicator() {
    List<Widget> indicators = [];
    for (int i = 0; i < 3; i++) {
      if (currentIndex == i) {
        indicators.add(_indicator(true));
      } else {
        indicators.add(_indicator(false));
      }
    }

    return indicators;
  }
}
