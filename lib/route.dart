import 'package:Covid_Diary/map/maps.dart';
import 'package:Covid_Diary/pages/HomePage.dart';
import 'package:Covid_Diary/pages/addImage.dart';
import 'package:Covid_Diary/sidebar/sidebar_layout.dart';
import 'package:Covid_Diary/user/forgot.dart';
import 'package:Covid_Diary/user/login.dart';
import 'package:Covid_Diary/user/signup.dart';
import 'package:Covid_Diary/user/wrapper.dart';
import 'package:flutter/material.dart';
import 'pages/intro.dart';

class RouteGenerator {
  // ignore: missing_return
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => Intro());
      case '/login':
        return MaterialPageRoute(builder: (_) => Login());
      case '/signup':
        return MaterialPageRoute(builder: (_) => SignUp());
      case '/forgot':
        return MaterialPageRoute(builder: (_) => Forgot());
      case '/home':
        return MaterialPageRoute(builder: (_) => HomePage());
      case '/SideBarLayout':
        return MaterialPageRoute(builder: (_) => SideBarLayout());
      case '/Map':
        return MaterialPageRoute(builder: (_) => Maps());
      case '/AddImage':
        return MaterialPageRoute(builder: (_) => AddImage());
      case '/Wrapper':
        return MaterialPageRoute(builder: (_) => Wrapper());
      default:
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: Text('ERROR'),
        ),
        body: Center(
          child: Text('ERROR'),
        ),
      );
    });
  }
}
