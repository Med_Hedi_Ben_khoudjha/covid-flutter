import 'package:Covid_Diary/models/user.dart';
import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

class UserList extends StatefulWidget {
  @override
  _UserList createState() => _UserList();
}

class _UserList extends State<UserList> {
  @override
  Widget build(BuildContext context) {
    final users = Provider.of<List<User>>(context);
    users.forEach((user) {
      print(user.name);
      print(user.email);
    });

    return Container();
  }
}
