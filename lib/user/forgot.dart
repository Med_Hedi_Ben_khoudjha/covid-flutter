import 'package:Covid_Diary/services/auth.dart';
import 'package:Covid_Diary/shared/loading.dart';

import 'package:Covid_Diary/user/login.dart';
import 'package:Covid_Diary/user/signup.dart';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import '../size_service.dart';

class Forgot extends StatefulWidget {
  @override
  _Forgot createState() => _Forgot();
}

class _Forgot extends State {
  final AuthService _auth = AuthService();
  final _formkey = GlobalKey<FormState>();
  String email = '';
  String password = '';
  String error = '';
  bool loading = false;
  @override
  Widget build(BuildContext context) {
    var scrWidth = MediaQuery.of(context).size.width;
    var scrHeight = MediaQuery.of(context).size.height;
    initSizeConfigGlobal(context);

    return loading
        ? Loading()
        : SafeArea(
            child: Scaffold(
              body: SingleChildScrollView(
                physics: BouncingScrollPhysics(),
                child: Stack(
                  children: [
                    Column(
                      children: [
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 40.0, top: 40),
                            child: Text(
                              'Forgot',
                              style: TextStyle(
                                fontFamily: 'Chilanka',
                                fontSize: 35,
                                color: Color(0xff0C2551),
                                fontWeight: FontWeight.w900,
                              ),
                            ),
                            //
                          ),
                        ),

                        SizedBox(
                          height: getSizeVerticallyScaled(140),
                        ),
                        SizedBox(
                          height: getSizeVerticallyScaled(65),
                          child: Text(
                            "Entrez votre adresse e-mail, et nous vous enverrons un lien pour récupérer votre compte.",
                            textAlign: TextAlign.center,
                            overflow: TextOverflow.clip,
                            style: TextStyle(
                              color: Colors.black,
                              fontFamily: 'Cardo',
                              height: (1.5),
                            ),
                          ),
                        ),
                        //

                        SizedBox(
                          height: getSizeVerticallyScaled(30),
                        ),

                        SizedBox(
                          height: getSizeVerticallyScaled(30),
                        ),
                        Container(
                            width: getSizeVerticallyScaled(400),
                            child: Form(
                                key: _formkey,
                                child: Column(
                                  children: <Widget>[
                                    TextFormField(
                                      validator: (value) {
                                        if (value.isEmpty) {
                                          return 'Please enter email';
                                        }
                                        return null;
                                      },
                                      onChanged: (val) {
                                        setState(() => email = val);
                                      },
                                      autocorrect: true,
                                      textAlign: TextAlign.left,
                                      decoration: InputDecoration(
                                          prefixIcon: Icon(Icons.email_rounded),
                                          isDense: true, // Added this
                                          contentPadding: EdgeInsets.all(
                                              getSizeVerticallyScaled(
                                                  20)), // Added this
                                          labelText: " Email",
                                          hintText: " Email",
                                          hintStyle: TextStyle(
                                              fontSize: 18,
                                              color: Colors.grey[350],
                                              fontWeight: FontWeight.w600),
                                          focusColor: Color(0xff0962ff),
                                          focusedBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            borderSide: BorderSide(
                                                color: Color(0xff0962ff)),
                                          ),
                                          enabledBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(20),
                                            borderSide: BorderSide(
                                              color: Colors.grey[350],
                                            ),
                                          )),
                                    ),
                                    SizedBox(
                                      height: getSizeVerticallyScaled(30),
                                    ),
                                    InkWell(
                                        child: Container(
                                          margin: EdgeInsets.symmetric(
                                              vertical: 20),
                                          width: scrWidth * 0.5,
                                          height: getSizeVerticallyScaled(70),
                                          decoration: BoxDecoration(
                                            color: Colors.indigo[200],
                                            borderRadius:
                                                BorderRadius.circular(40),
                                          ),
                                          child: Center(
                                            child: Text(
                                              'Send',
                                              style: TextStyle(
                                                fontFamily: 'ProductSans',
                                                fontSize: 20,
                                                fontWeight: FontWeight.w100,
                                                color: Colors.white,
                                              ),
                                            ),
                                          ),
                                        ),
                                        onTap: () async {
                                          if (_formkey.currentState
                                              .validate()) {
                                            setState(() => loading = true);

                                            _auth
                                                .sendPasswordResetEmail(
                                                    email.trim())
                                                .whenComplete(() {
                                              print(email);

                                              Navigator.of(context).push(
                                                MaterialPageRoute(
                                                  builder: (_) => Login(),
                                                ),
                                              );
                                            });
                                          } else {
                                            setState(() {
                                              error =
                                                  'Could not Sign In With Those Credentials';
                                              loading = false;
                                            });
                                          }

                                          //Navigator.push(
                                          //context,
                                          // MaterialPageRoute(
                                          // builder: (context) => SideBarLayout()));
                                        }),
                                    SizedBox(
                                      height: getSizeVerticallyScaled(30),
                                    ),
                                    Text(
                                      error,
                                      style: TextStyle(
                                          color: Colors.red,
                                          fontSize: 14,
                                          fontFamily: 'Product Sans'),
                                    ),
                                  ],
                                ))),

                        SizedBox(
                          height: getSizeVerticallyScaled(30),
                        ),
                        //
                        RichText(
                          text: TextSpan(
                            children: [
                              TextSpan(
                                text: 'Don t have account? ',
                                style: TextStyle(
                                  fontFamily: 'Product Sans',
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black.withOpacity(0.6),
                                ),
                              ),
                              TextSpan(
                                text: 'Sign Up',
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () => Navigator.push(
                                        context,
                                        PageRouteBuilder(
                                            transitionsBuilder: (BuildContext
                                                    context,
                                                Animation<double> animation,
                                                Animation<double> secanimtion,
                                                Widget child) {
                                              animation = CurvedAnimation(
                                                  parent: animation,
                                                  curve: Curves.elasticInOut);
                                              return ScaleTransition(
                                                alignment: Alignment.center,
                                                scale: animation,
                                                child: child,
                                              );
                                            },
                                            transitionDuration:
                                                Duration(seconds: 1),
                                            pageBuilder: (BuildContext context,
                                                Animation<double> animation,
                                                Animation<double> secanimtion) {
                                              return SignUp();
                                            }),
                                      ),
                                style: TextStyle(
                                  fontFamily: 'Product Sans',
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.indigo[200],
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                    ClipPath(
                      clipper: OuterClippedPart(),
                      child: Container(
                        color: Colors.indigo[400],
                        width: scrWidth,
                        height: scrHeight,
                      ),
                    ),
                    //
                    ClipPath(
                      clipper: InnerClippedPart(),
                      child: Container(
                        color: Colors.indigo[200],
                        width: scrWidth,
                        height: scrHeight,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
  }
}

// ignore: must_be_immutable
// ignore: camel_case_types
// ignore: must_be_immutable
class Neu_button extends StatelessWidget {
  Neu_button({this.char});
  String char;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: getSizeVerticallyScaled(50),
      height: getSizeVerticallyScaled(50),
      decoration: BoxDecoration(
        color: Color(0xffffffff),
        borderRadius: BorderRadius.circular(13),
        boxShadow: [
          BoxShadow(
            offset: Offset(12, 11),
            blurRadius: 20,
            color: Colors.lightBlue.withOpacity(0.1),
          )
        ],
      ),
      //
      child: Center(
        child: Text(
          char,
          style: TextStyle(
            fontFamily: 'ProductSans',
            fontSize: 26,
            fontWeight: FontWeight.bold,
            color: Colors.lightBlue,
          ),
        ),
      ),
    );
  }
}

class OuterClippedPart extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();
    //
    path.moveTo(size.width / 2, 0);
    path.lineTo(size.width, 0);
    path.lineTo(size.width, size.height / 4);
    //
    path.cubicTo(size.width * 0.55, size.height * 0.16, size.width * 0.85,
        size.height * 0.05, size.width / 2, 0);
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }
}

class InnerClippedPart extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();
    //
    path.moveTo(size.width * 0.7, 0);
    path.lineTo(size.width, 0);
    path.lineTo(size.width, size.height * 0.1);
    //
    path.quadraticBezierTo(
        size.width * 0.8, size.height * 0.11, size.width * 0.7, 0);

    //
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }
}
