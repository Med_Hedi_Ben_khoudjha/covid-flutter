import 'package:Covid_Diary/bloc.navigation_bloc/navigation_bloc.dart';
import 'package:Covid_Diary/models/user.dart';

import 'package:Covid_Diary/sidebar/sidebar_layout.dart';
import 'package:Covid_Diary/user/login.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

class Wrapper extends StatefulWidget {
  @override
  _Wrapper createState() => _Wrapper();
}

class _Wrapper extends State<Wrapper> {
  FirebaseMessaging _firebasemessaging = FirebaseMessaging();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _firebasemessaging.configure(onMessage: (message) async {
      BlocProvider.of<NavigationBloc>(context)
          .add(NavigationEvents.StatClickedEvent);
    });
  }

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);
    if (user == null) {
      return Login();
    } else
      return SideBarLayout();
  }
}
