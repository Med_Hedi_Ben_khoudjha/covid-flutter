import 'package:flutter/material.dart';

class AppColors {
  static final Color backgroundColor = Colors.black;
  static final Color mainColor = Color(0XFF8d12fe);
}
