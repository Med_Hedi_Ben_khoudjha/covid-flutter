import 'package:Covid_Diary/map/maps.dart';
import 'package:Covid_Diary/pages/HomePage.dart';
import 'package:Covid_Diary/pages/history.dart';
import 'package:Covid_Diary/pages/stat.dart';
import 'package:Covid_Diary/pages/suivi.dart';
import 'package:bloc/bloc.dart';
import '../pages/EditProfilePage.dart';

enum NavigationEvents {
  HomePageClickedEvent,
  EditProfilePageClickedEvent,
  MapClickedEvent,
  SuiviClickedEvent,
  HistoryClickedEvent,
  StatClickedEvent,
}

abstract class NavigationStates {}

class NavigationBloc extends Bloc<NavigationEvents, NavigationStates> {
  @override
  NavigationStates get initialState => HomePage();

  @override
  Stream<NavigationStates> mapEventToState(NavigationEvents event) async* {
    switch (event) {
      case NavigationEvents.HomePageClickedEvent:
        yield HomePage();
        break;
      case NavigationEvents.EditProfilePageClickedEvent:
        yield EditProfilePage();
        break;
      case NavigationEvents.MapClickedEvent:
        yield Maps();
        break;
      case NavigationEvents.SuiviClickedEvent:
        yield Suivi();
        break;
      case NavigationEvents.HistoryClickedEvent:
        yield History();
        break;
      case NavigationEvents.StatClickedEvent:
        yield Stat();
        break;
    }
  }
}
