class Strings {
  static var stepOneTitle = "Coronavirus";
  static var stepOneContent =
      "Les coronavirus regroupent de nombreux virus dont seuls certains sont pathogènes pour l'Homme, parfois mortels.";
  static var stepTwoTitle = "Protection";
  static var stepTwoContent =
      "Portez un masque Lavez-vous les mains Respectez la distanciation physique";
  static var stepThreeTitle = "Fight Covid-19";
  static var stepThreeContent =
      "Signaler votre état quotidiennement est crucial pour créer une heatmap prévisionnelle des foyers du virus.Ces données fourniront aux autorités médicales un aperçu en temps réel des épidémies potentielles.";
}
