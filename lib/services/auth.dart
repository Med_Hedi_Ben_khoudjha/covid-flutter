import 'package:Covid_Diary/models/user.dart';
import 'package:Covid_Diary/services/database.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthService {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  //create user obj based on FirebaseUser
  User _userFromFirebaseUSer(FirebaseUser user) {
    return user != null ? User(uid: user.uid, email: user.email) : null;
  }

  //auth change user stream
  Stream<User> get user {
    return _auth.onAuthStateChanged
        //.map((FirebaseUser user) => _userFromFirebaseUSer(user));
        .map(_userFromFirebaseUSer);
  }

  Future newpsw(String newPassword) async {
    FirebaseUser user = await _auth.currentUser();

    if (user != null) {
      user.updatePassword(newPassword);
    }
  }

  // ignore: unused_element
  Future<void> _changePassword(String password) async {
    //Create an instance of the current user.
    FirebaseUser user = await FirebaseAuth.instance.currentUser();

    //Pass in the password to updatePassword.
    user.updatePassword(password).then((_) {
      print("Succesfull changed password");
    }).catchError((error) {
      print("Password can't be changed" + error.toString());
      //This might happen, when the wrong password is in, the user isn't found, or if the user hasn't logged in recently.
    });
  }

  Future<bool> validatePassword(String password) async {
    FirebaseUser user = await _auth.currentUser();

    var authCredentials =
        EmailAuthProvider.getCredential(email: user.email, password: password);
    try {
      var authResult = await user.reauthenticateWithCredential(authCredentials);
      return authResult.user != null;
    } catch (e) {
      print(e);
      return false;
    }
  }

  Future<void> updatePassword(String password) async {
    FirebaseUser user = await _auth.currentUser();
    user.updatePassword(password);
  }

  //Sign in
  Future signIn() async {
    try {
      AuthResult result = await _auth.signInAnonymously();
      FirebaseUser user = result.user;
      return _userFromFirebaseUSer(user);
    } catch (e) {
      print(e);
      return null;
    }
  }

  //Sign in with email & password
  Future signInWithemailAndPassword(String email, String password) async {
    try {
      AuthResult result = await _auth.signInWithEmailAndPassword(
          email: email, password: password);
      FirebaseUser user = result.user;
      SharedPreferences prefs = await SharedPreferences.getInstance();

      prefs.setString('email', user.email);
      prefs.setString('uid', user.uid);
      return _userFromFirebaseUSer(user);
    } catch (e) {
      print(e);
    }
  }

  //get password
  Future sendPasswordResetEmail(String email) async {
    return _auth.sendPasswordResetEmail(email: email);
  }

//Get UID
  getCurrentUID() async {
    return (await _auth.currentUser());
  }

  //register with email & password
  Future registerWithemailAndPassword(
      String email, String password, String name) async {
    try {
      AuthResult result = await _auth.createUserWithEmailAndPassword(
          email: email, password: password);
      FirebaseUser user = result.user;
      await DatabaseService(uid: user.uid).updateUserData(name, email);
      SharedPreferences prefs = await SharedPreferences.getInstance();

      prefs.setString('email', user.email);
      prefs.setString('uid', user.uid);
      return _userFromFirebaseUSer(user);
    } catch (e) {
      print(e);
    }
  }

  final GoogleSignIn googleSignIn = new GoogleSignIn();

  Future signInWithGoogle() async {
    final GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
    final GoogleSignInAuthentication googleSignInAuthentication =
        await googleSignInAccount.authentication;

    final AuthCredential credential = GoogleAuthProvider.getCredential(
      accessToken: googleSignInAuthentication.accessToken,
      idToken: googleSignInAuthentication.idToken,
    );

    final AuthResult authResult = await _auth.signInWithCredential(credential);
    final FirebaseUser user = authResult.user;
    print(user.displayName.toString());
    print(user.photoUrl.toString());
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('photo', user.photoUrl);
    prefs.setString('email', user.email);
    prefs.setString('uid', user.uid);
    print(prefs.getString('photo'));
    assert(!user.isAnonymous);
    assert(await user.getIdToken() != null);

    final FirebaseUser currentUser = await _auth.currentUser();
    assert(user.uid == currentUser.uid);

    return _userFromFirebaseUSer(user);
  }

  //sign out
  void signOut() {
    _auth.signOut();
  }

  void signOutg() {
    googleSignIn.signOut();
    print("User Sign Out");
  }

  final GoogleSignIn _googleSignIn = GoogleSignIn();

  final Firestore _db = Firestore.instance;
  Observable<FirebaseUser> users; // firebase user
  Observable<Map<String, dynamic>> profile; // custom user data in Firestore
  PublishSubject loading = PublishSubject();

  AuthService() {
    users = Observable(_auth.onAuthStateChanged);

    profile = users.switchMap((FirebaseUser u) {
      if (u != null) {
        return _db
            .collection('users')
            .document(u.uid)
            .snapshots()
            .map((snap) => snap.data);
      } else {
        return Observable.just({});
      }
    });
  }

  void updateUserData(FirebaseUser user) async {
    DocumentReference ref = _db.collection('users').document(user.uid);

    return ref.setData({
      'uid': user.uid,
      'email': user.email,
      'photoURL': user.photoUrl,
      'displayName': user.displayName,
      'lastSeen': DateTime.now()
    }, merge: true);
  }
}
