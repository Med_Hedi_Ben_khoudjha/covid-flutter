import 'package:Covid_Diary/models/user.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class DatabaseService {
  final String uid;
  DatabaseService({this.uid});
  final CollectionReference form = Firestore.instance.collection('user');
  final CollectionReference suivi = Firestore.instance.collection('suivi');
//collection user reference
  Future updateUserData(String name, String email) async {
    return await form.document(uid).setData({'name': name, 'email': email});
  }

//collection user reference
  Future updateUserSuivi(String latitude, String longitude, String app) async {
    return await suivi
        .document(uid)
        .setData({'latitude': latitude, 'longitude': longitude, 'app': app});
  }

  Future getData() async {
    QuerySnapshot querySnapshot =
        await Firestore.instance.collection("suivi").getDocuments();
    List<DocumentSnapshot> list = querySnapshot.documents;
    return list;
  }

//users list from snapshot
  List<User> _userList(QuerySnapshot snapshot) {
    return snapshot.documents.map((doc) {
      return User(email: doc.data['email'] ?? '', name: ['name'] ?? '');
    }).toList();
  }

  //get user stream
  Stream<List<User>> get users {
    return form.snapshots().map(_userList);
  }
}
