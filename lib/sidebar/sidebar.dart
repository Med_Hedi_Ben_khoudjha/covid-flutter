import 'dart:async';
import 'dart:io';

import 'package:Covid_Diary/services/auth.dart';
import 'package:Covid_Diary/user/login.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../bloc.navigation_bloc/navigation_bloc.dart';
import '../sidebar/menu_item.dart';
import '../size_service.dart';

class SideBar extends StatefulWidget {
  @override
  _SideBarState createState() => _SideBarState();
}

class _SideBarState extends State<SideBar>
    with SingleTickerProviderStateMixin<SideBar> {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  FirebaseUser user;
  String _uploadedFileURL = 'https://picsum.photos/250?image=9';
  String _uploadedFileURL2 = 'https://picsum.photos/250?image=9';
  String email = '';
  File imageFile;

  Future<bool> _exitApp(BuildContext context) {
    final AuthService _auth = AuthService();

    return showDialog(
          context: context,
          barrierColor: Colors.black,
          child: AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)),
            backgroundColor: Colors.white,
            elevation: 0,
            title: Center(
              child: Column(children: [
                CircleAvatar(
                  backgroundColor: Colors.grey,
                  backgroundImage: _uploadedFileURL != null
                      ? NetworkImage(_uploadedFileURL)
                      : NetworkImage(_uploadedFileURL2),
                ),
                Text(
                  'Do you want to exit this application?',
                  style: TextStyle(fontSize: 14),
                )
              ]),
            ),
            content: Text(
              'We hate to see you leave...',
              style: TextStyle(fontSize: 12),
            ),
            actions: <Widget>[
              FlatButton(
                textColor: Colors.black,
                onPressed: () {
                  print("you choose no");
                  Navigator.of(context, rootNavigator: true).pop();
                },
                child: Text('No'),
              ),
              FlatButton(
                textColor: Colors.black,
                onPressed: () async {
                  SharedPreferences prefs =
                      await SharedPreferences.getInstance();
                  prefs.remove('email');
                  prefs.remove('photo');
                  _auth.signOut();
                  _auth.signOutg();

                  Navigator.of(context, rootNavigator: true).push(
                    MaterialPageRoute(
                      builder: (_) => Login(),
                    ),
                  );
                },
                child: Text('Yes'),
              ),
            ],
          ),
        ) ??
        false;
  }

  Future<dynamic> loadImage() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    print(prefs.getString('uid') + "sidebar");
    String uid = prefs.getString('uid');
    var user = await FirebaseAuth.instance.currentUser();
    print(user.uid);

    final ref = FirebaseStorage.instance.ref().child('${user.uid}');
// no need of the file extension, the name will do fine.
    String url = await ref.getDownloadURL();
    //
    if (url != null) {
      print(url);
      setState(() {
        _uploadedFileURL = url;
        email = user.email;
      });
      prefs.setString('photo', url);
    }
  }

  initUser() async {
    var user = await FirebaseAuth.instance.currentUser();
    //  SharedPreferences prefs = await SharedPreferences.getInstance();
//    prefs.getString('photo');
    //print(prefs.getString('photo'));
//    setState(() {
    //    _uploadedFileURL2 = prefs.getString('photo');
    // });

    setState(() {
      _uploadedFileURL = user.photoUrl;
      email = user.email;
    });
    setState(() {});
  }

  AnimationController _animationController;
  StreamController<bool> isSidebarOpenedStreamController;
  Stream<bool> isSidebarOpenedStream;
  StreamSink<bool> isSidebarOpenedSink;
  final _animationDuration = const Duration(milliseconds: 500);

  @override
  void initState() {
    super.initState();
    _animationController =
        AnimationController(vsync: this, duration: _animationDuration);
    isSidebarOpenedStreamController = PublishSubject<bool>();
    isSidebarOpenedStream = isSidebarOpenedStreamController.stream;
    isSidebarOpenedSink = isSidebarOpenedStreamController.sink;
    initUser();
    loadImage();
  }

  @override
  void dispose() {
    _animationController.dispose();
    isSidebarOpenedStreamController.close();
    isSidebarOpenedSink.close();
    super.dispose();
  }

  void onIconPressed() {
    final animationStatus = _animationController.status;
    final isAnimationCompleted = animationStatus == AnimationStatus.completed;

    if (isAnimationCompleted) {
      isSidebarOpenedSink.add(false);
      _animationController.reverse();
    } else {
      isSidebarOpenedSink.add(true);
      _animationController.forward();
    }
  }

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    initSizeConfigGlobal(context);

    return StreamBuilder<bool>(
      initialData: false,
      stream: isSidebarOpenedStream,
      builder: (context, isSideBarOpenedAsync) {
        return AnimatedPositioned(
          duration: _animationDuration,
          top: 0,
          bottom: 0,
          left: isSideBarOpenedAsync.data ? 0 : -screenWidth,
          right: isSideBarOpenedAsync.data ? 0 : screenWidth - 32,
          child: Row(
            children: <Widget>[
              Expanded(
                child: Container(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  color: Colors.white,
                  child: Column(
                    children: <Widget>[
                      SizedBox(
                        height: getSizeVerticallyScaled(50),
                      ),
                      Container(
                        child: ListTile(
                          title: Text(
                            "Covid Diary",
                            style: TextStyle(
                                color: Colors.indigo[400],
                                fontSize: 20,
                                fontWeight: FontWeight.w800),
                          ),
                          subtitle: Text(
                            email,
                            style: TextStyle(
                              color: Colors.indigo[200],
                              fontSize: 13,
                            ),
                          ),
                          leading: CircleAvatar(
                            radius: 30.0,
                            backgroundColor: Colors.transparent,
                            backgroundImage: _uploadedFileURL != null
                                ? NetworkImage(_uploadedFileURL)
                                : NetworkImage(_uploadedFileURL2),
                          ),
                        ),
                      ),
                      Divider(
                        height: getSizeVerticallyScaled(64),
                        thickness: 0.5,
                        color: Colors.indigo,
                        indent: 32,
                        endIndent: 32,
                      ),
                      MenuItem(
                        icon: Icons.home,
                        title: "Home",
                        onTap: () {
                          onIconPressed();

                          BlocProvider.of<NavigationBloc>(context)
                              .add(NavigationEvents.HomePageClickedEvent);
                        },
                      ),
                      MenuItem(
                        icon: Icons.edit,
                        title: "Edit Profile",
                        onTap: () {
                          onIconPressed();
                          BlocProvider.of<NavigationBloc>(context).add(
                              NavigationEvents.EditProfilePageClickedEvent);
                        },
                      ),
                      MenuItem(
                          icon: Icons.card_giftcard,
                          title: "Daily Monitoring",
                          onTap: () {
                            onIconPressed();
                            BlocProvider.of<NavigationBloc>(context)
                                .add(NavigationEvents.SuiviClickedEvent);
                          }),
                      Divider(
                        height: getSizeVerticallyScaled(64),
                        thickness: 0.5,
                        color: Colors.indigo,
                        indent: 32,
                        endIndent: 32,
                      ),
                      MenuItem(
                        icon: Icons.history,
                        title: "History Diary",
                        onTap: () {
                          onIconPressed();
                          BlocProvider.of<NavigationBloc>(context)
                              .add(NavigationEvents.HistoryClickedEvent);
                        },
                      ),
                      MenuItem(
                        icon: Icons.stacked_bar_chart,
                        title: "Stat",
                        onTap: () {
                          onIconPressed();
                          BlocProvider.of<NavigationBloc>(context)
                              .add(NavigationEvents.StatClickedEvent);
                        },
                      ),
                      MenuItem(
                        icon: Icons.contact_support,
                        title: "Contact US",
                        onTap: () {
                          onIconPressed();
                          BlocProvider.of<NavigationBloc>(context)
                              .add(NavigationEvents.MapClickedEvent);
                        },
                      ),
                      MenuItem(
                        icon: Icons.exit_to_app,
                        title: "Logout",
                        onTap: () async {
                          _exitApp(context);
                        },
                      ),
                    ],
                  ),
                ),
              ),
              Align(
                alignment: Alignment(0, -0.9),
                child: GestureDetector(
                  onTap: () {
                    onIconPressed();
                  },
                  child: ClipPath(
                    clipper: CustomMenuClipper(),
                    child: Container(
                      width: getSizeVerticallyScaled(35),
                      height: getSizeVerticallyScaled(110),
                      color: Colors.black,
                      alignment: Alignment.centerLeft,
                      child: AnimatedIcon(
                        progress: _animationController.view,
                        icon: AnimatedIcons.menu_close,
                        color: Colors.white,
                        size: 25,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}

class CustomMenuClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Paint paint = Paint();
    paint.color = Colors.white;

    final width = size.width;
    final height = size.height;

    Path path = Path();
    path.moveTo(0, 0);
    path.quadraticBezierTo(0, 8, 10, 16);
    path.quadraticBezierTo(width - 1, height / 2 - 20, width, height / 2);
    path.quadraticBezierTo(width + 1, height / 2 + 20, 10, height - 16);
    path.quadraticBezierTo(0, height - 8, 0, height);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }
}
