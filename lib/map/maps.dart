import 'package:Covid_Diary/bloc.navigation_bloc/navigation_bloc.dart';
import 'package:Covid_Diary/size_service.dart';
import 'package:contactus/contactus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:latlong/latlong.dart';

class Maps extends StatefulWidget with NavigationStates {
  @override
  _Maps createState() => _Maps();
}

class _Maps extends State<Maps> {
  Widget build(BuildContext context) {
    initSizeConfigGlobal(context);
    LatLng _center = LatLng(36.8522391, 10.2077839);
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
          height: size.height,
          child: Stack(
            children: [
              Positioned(
                top: 0,
                left: 0,
                child: Image.asset(
                  "assets/images/main_top.png",
                  width: size.width * 0.3,
                ),
              ),
              Positioned(
                bottom: 0,
                left: 0,
                child: Image.asset(
                  "assets/images/main_bottom.png",
                  width: size.width * 0.2,
                ),
              ),
              ListView(children: [
                SizedBox(
                  height: getSizeVerticallyScaled(35),
                ),
                Container(
                  padding: EdgeInsets.only(left: 43),
                  child: Text(
                    "Contact US",
                    style: TextStyle(
                        color: Colors.lightBlue,
                        fontSize: 30,
                        fontFamily: 'Chilanka',
                        fontWeight: FontWeight.w900),
                  ),
                ),
                SizedBox(
                  height: getSizeVerticallyScaled(30),
                ),
                new Text("Contact Us to learn how we can help you",
                    style: TextStyle(
                      fontSize: 13,
                      fontFamily: 'Cardo',
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                    ),
                    textAlign: TextAlign.center),
                SizedBox(
                  height: getSizeVerticallyScaled(50),
                ),
                new Column(

                    //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      new Container(
                        height: 350.0,
                        child: FlutterMap(
                          options: new MapOptions(
                            minZoom: 15.0,
                            center: _center,
                            interactive: true,
                          ),
                          layers: [
                            new TileLayerOptions(
                                urlTemplate:
                                    "https://api.mapbox.com/styles/v1/medcabiste/ckhbz0r9e0zf419rj58vxyzyc/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1IjoibWVkY2FiaXN0ZSIsImEiOiJjanRudmxqNDMwZ3J6NDRuMm5qZ2Zka2F0In0.YlvlWPvJrQny19UOUWdmjA",
                                subdomains: [
                                  'a',
                                  'b',
                                  'c'
                                ],
                                additionalOptions: {
                                  'accessToken':
                                      'sk.eyJ1IjoibWVkY2FiaXN0ZSIsImEiOiJja2hieHlnMDkwMm9tMnN0Z24yZWpxOWd2In0.JEHu3uLOaDbVLj_C8dLq9A',
                                  'id': 'mapbox.streets'
                                }),
                            new MarkerLayerOptions(
                              markers: [
                                new Marker(
                                  width: 80,
                                  height: 80,
                                  point: _center,
                                  builder: (ctx) => Container(
                                      child: IconButton(
                                    icon: Icon(
                                      FontAwesomeIcons.mapMarkerAlt,
                                      color: Colors.red,
                                      size: 40,
                                    ),
                                    onPressed: () {
                                      showModalBottomSheet(
                                          context: context,
                                          builder: (builder) {
                                            return Container(
                                              margin: EdgeInsets.all(20),
                                              color: Colors.white,
                                              child: new ContactUs(
                                                // logo: AssetImage('assets/images/l.png'),
                                                email:
                                                    'mohamedhedi.benkhoudja@esprit.tn',
                                                companyName: 'Fight Covid-19',
                                                phoneNumber: '+216 21 267 061',
                                                website: 'https://esprit.tn/',
                                                tagLine: 'Contact US',
                                                companyFontSize: 20,
                                                companyColor: Colors.white,
                                                cardColor: Colors.white,
                                                taglineColor: Colors.black,
                                                textColor: Colors.black,
                                              ),
                                            );
                                          });
                                    },
                                  )),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: getSizeVerticallyScaled(15),
                      ),
                      new Text(
                          "For more information please click on the marker",
                          style: TextStyle(
                            fontSize: 15,
                            fontFamily: 'Cardo',
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                          ),
                          textAlign: TextAlign.center),
                      Divider(
                        height: getSizeVerticallyScaled(64),
                        thickness: 0.5,
                        color: Colors.lightBlue,
                        indent: 32,
                        endIndent: 32,
                      ),
                    ]),
                SizedBox(
                  height: getSizeVerticallyScaled(35),
                ),
                Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: new Text(
                      "Email us with any questions or inquiries or call +216 21 267 061.We would be happy to answer tour questions ans set up a meeting with you.Fight covid-19 can help set you apart from the flock!",
                      style: TextStyle(
                        fontSize: 13,
                        fontFamily: 'Cardo',
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                      ),
                      textAlign: TextAlign.center),
                ),
                SizedBox(
                  height: getSizeVerticallyScaled(35),
                ),
              ]),
            ],
          )),
    );
  }
}
