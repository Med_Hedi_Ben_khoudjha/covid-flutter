import 'package:Covid_Diary/route.dart';
import 'package:Covid_Diary/services/auth.dart';
import 'package:Covid_Diary/sidebar/sidebar_layout.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'pages/intro.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences prefs = await SharedPreferences.getInstance();
  var email = prefs.getString('email');

  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: email == null ? MyApp() : SideBarLayout(),
  ));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Future<bool> _onBackPressed() {
      return showDialog(
            context: context,
            builder: (context) => new AlertDialog(
              title: new Text('Are you sure?'),
              content: new Text('Do you want to exit an App'),
              actions: <Widget>[
                new GestureDetector(
                  onTap: () => Navigator.of(context, rootNavigator: true).pop(),
                  child: Text("NO"),
                ),
                SizedBox(height: 16),
                new GestureDetector(
                  onTap: () => Navigator.of(context).pop(true),
                  child: Text("YES"),
                ),
              ],
            ),
          ) ??
          false;
    }

    return new WillPopScope(
        onWillPop: _onBackPressed,
        child: StreamProvider.value(
          value: AuthService().user,
          child: MaterialApp(
              debugShowCheckedModeBanner: false,
              initialRoute: ('/'),
              onGenerateRoute: RouteGenerator.generateRoute,
              home: AnimatedSplashScreen(
                  duration: 3000,
                  splashIconSize: 3000,
                  splash: 'assets/images/l.png',
                  nextScreen: Intro(),
                  splashTransition: SplashTransition.fadeTransition,
                  pageTransitionType: PageTransitionType.scale,
                  backgroundColor: Colors.white)),
        ));
  }
}
